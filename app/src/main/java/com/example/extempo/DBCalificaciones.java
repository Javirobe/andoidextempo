package com.example.extempo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

public class DBCalificaciones {

    private Context context;
    private DataBaseOpenHelper connhelper;
    private SQLiteDatabase db;


    public DBCalificaciones(Context context) {
        this.context = context;
        this.connhelper = new DataBaseOpenHelper(this.context);
        openDatabase();
    }

    public long insertCalificacion(Calficacion calficacion) {
        ContentValues values = new ContentValues();
        values.put(Tablas.CalificacionesTabla.calificacion, calficacion.getCalficiacion());
        values.put(Tablas.CalificacionesTabla.grupo, calficacion.getGrupo());
        values.put(Tablas.CalificacionesTabla.materia, calficacion.getMateria());
        values.put(Tablas.CalificacionesTabla.matricula, calficacion.getMatircula());

        return db.insert(Tablas.CalificacionesTabla.TableName, null, values);
    }



    private String[] COLUMS = {
            Tablas.CalificacionesTabla.calificacion,
            Tablas.CalificacionesTabla.grupo,
            Tablas.CalificacionesTabla.materia,
            Tablas.CalificacionesTabla.matricula
    };


    private Calficacion read(Cursor cursor) {
        Calficacion calficacion = new Calficacion();
        calficacion.setCalficiacion(cursor.getFloat(0));
        calficacion.setGrupo(cursor.getString(1));
        calficacion.setMateria(cursor.getString(2));
        calficacion.setMatircula(cursor.getString(3));
        return calficacion;
    }

    public ArrayList<Calficacion> getCalificaciones() {
        ArrayList<Calficacion> list = new ArrayList<>();
        Cursor cursor = db.query(Tablas.CalificacionesTabla.TableName, COLUMS, null, null, null, null, null);

        if (cursor.getCount() == 0) {
            return list;
        }

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            list.add(read(cursor));
            cursor.moveToNext();
        }
        cursor.close();
        return list;
    }


    public void close() {
        connhelper.close();
    }

    public void openDatabase() {
        db = connhelper.getWritableDatabase();
    }


}
